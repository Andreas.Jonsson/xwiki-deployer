
XWiki Deployer
==============


# Installation

cabal configure
cabal build
cabal test
cabal install

# Creating a project

The project file is a JSON file containing an object with the following properties:

* **url** - The base URL of the XWiki instance.
* **credentials** - Filen name of credentials file.
* **wiki** - The database name of the XWiki instance.
* **backupExcludeSpaces** - Spaces (parent pages) to exclude for the xar-file backup functionality.
* **entities** - Array of entities to deploy.  Entities have the following properties:
    * **file** - The file name of the local file that holds the contents of the entity

    * **entity** - The entity to deploy to.  This can be a page name or a
      property name, for instance:
      XWiki.XWikiDefaultSkin^XWiki.StyleSheetExtension[0].code